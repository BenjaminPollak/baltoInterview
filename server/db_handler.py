#!/usr/bin/env python3
import sqlite3

class db_handler:

    def __init__(self, db):
        self.conn = sqlite3.connect(db)
        self.cur = self.conn.cursor()
