#!/usr/bin/env python3
#runs on http://127.0.0.1:5000
from flask import Flask, render_template, jsonify

import db_handler

app = Flask(__name__, static_folder='../static/dist', template_folder='../static/public')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/get_phones')
def get_phones():
    dbh = db_handler.db_handler("../media.sqlite")
    data = dbh.cur.execute("select phone from Customer where phone is not null")
    data_str = ''
    phone_numbers = []
    for line in data:
        if(line):
            print(line[0])
            #data_str = data_str + line[0]  + '\n'
            phone_numbers.append(line[0])
    return jsonify(result=phone_numbers)

if __name__ == '__main__':
    app.run()
