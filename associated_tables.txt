MUSIC
-----------------------------------
album has: AlbumId, Title, ArtistdId
artist has: ArtistId, Name
genre has: GenreId, Name
mediaType has: MediaTypeId, Name
playList has: PlaylistId, Name
playListTrack has: PlayListId, TrackId
track has: TrackId, Name, AlbumId, MediaTypeId, GenreId, Composer, Millisecon, Bytes, UnitPrice

Customer
-----------------------------------
customer has: CustomerId, FirstName, LastName, Company, Address, City, State, country, PostalCode, Phone, Fax, Email, SupportRepId
employee has: EmployeeId, LastName, FirstName, Title, ReportsTo, BirthDate, HireDate, Address, City, State, Country, PostalCode, Phone, Fax, Email
invoice has: InvoiceId, CustomerId, InvoiceDate, billinAddress, Billing city, BillingState, BillingCountry, BillingPostalCode, Total
invoiceLine has: InvoiceLineId, InvoiceId, TrackId, UnitPrice, Quantity
