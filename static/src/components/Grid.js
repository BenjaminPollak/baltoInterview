import React, { PureComponent } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Handle from './Handle'
import Delay from './Delay'

class Grid extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            phone_numbers: []
        };
    }

    componentWillMount() {
        // TODO: get phone numbers
    }
    render() {
        if(!this.state.phone_numbers) return (<p> loading </p>)
        return (
            <div>
                <Link to="/handle">
                    <button type="Time" >
                        <h1>Handle</h1>
                    </button>
                </Link>
                
                <Link to="/delay">
                    <button type="Time" >
                        <h1>Delay</h1>
                    </button>
                </Link>
                
                <Route path="/Handle" component={Handle}/>
                <Route path="/Delay" component={Delay}/>
                
                <ul>
                </ul>
            </div>
        );
    }
}

export default Grid;
