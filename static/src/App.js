import React, { Component } from 'react';

import './App.css';
import Grid from './components/Grid.js'

class App extends Component {
  render() {
    return (
      <div className="App">
		
        <header className="App-header">
          <h1 className="App-title">BALTO SOFTWARE PROJECT</h1>
        </header>
		
        <h1 className="App-intro">
          You have an incoming call!
        </h1>
		
		<Grid/>
		
      </div>
    );
  }
}

export default App;
